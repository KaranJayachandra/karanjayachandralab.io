# Hi! Welcome to my home on the internet

![profile](profile.jpg)

Signal Processing Engineer  
Systems and Applications - Algorithms  
NXP Semiconductors, The Netherlands

## About Me

I am an Engineer wandering across the [pale blue dot](https://en.wikipedia.org/wiki/Pale_Blue_Dot) constantly in search of the next interesting opportunity to learn. On this page, you can have a look at my bio, my qualifications or browse through my blog which contains both technical and non-technical content.

My guiding principles in life are [Rationalism](https://en.wikipedia.org/wiki/Rationalism) and [Humanism](https://en.wikipedia.org/wiki/Humanism). I believe in striving to push the boundaries of science as described wonderfully by [Matt Might](https://matt.might.net/articles/phd-school-in-pictures). But this push is matched by the need to innovate for a better future in more tangible way. I am always looking for new avenues to learn and grow in all aspect of life.

I enjoy sports, especially [badminton](https://badmintonnederland.toernooi.nl/player-profile/e76309d9-12b4-403e-b57c-39765194dd04) and have an on-again, off-again relationship with running. I also play snooker and chess on the wonderful [Lichess](https://lichess.org/@/KaranJayachandra) platform. My taste in music is mostly rock, metal and hip-hop. I am a self-taught campfire guitarist but hope to ditch the campfire title. I read both fiction and non-fiction titles when time permits. Authors that I like include Stephen King, John Grisham and Brandon Sanderson.

## The road so far

I am a [Tamilian](https://en.wikipedia.org/wiki/Tamils) born in Hyderabad and brought up in Kolar. I am naturally inquisitive, an unapologetic sceptic and have a thirst for knowledge. My Alma mater includes Amrita University and TU Delft from where I got my bachelor's and master's degrees in electrical engineer respectively. I have worked as an intern in the Deference Research and Development Organization of India and as a consultant for SAP. I am currently working on the next generation of Automotive Radar systems for Adaptive Cruise Control, Lane Change Assist, Blind Spot Detection and Cross Traffic Alert at NXP Semiconductors.
