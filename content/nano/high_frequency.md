+++
title = 'Low quality high frequency'
date = "2024-12-02"
+++

# Low quality high frequency

I have always enjoyed reading. My parents always encouraged my reading habit when I was a child and that stuck with me. With that came an appreciation of good writing. As an child brought up in rural India, I was above average when it came to writing for school. But as I grew up, I realized how woefully incompetent I was at. This was demotivating and resulted in me to stop trying. So for the last 10 years or so, I didn't put pen and paper together other for writing for work or school. All this changed recently. First, I turned 30 recently which was a wake up call of sorts. I suddenly didn't care anymore for if I was incompetent or not. All I wanted to say to myself when I was 40 was that I had given it an honest to god shot. Secondly, every time I saw good writing, I was envious. I was envious of how this author was always going to be better than me at something that I cared about.

While thinking about this, I realized something. I play badminton and can call myself objectively an advanced player without hubris. The advice I would give to a person that just started without doubt would be to just start and play as much as possible. Ask the people who are better than you for advice and try to one up yourself everyday. Then it hit me, that these authors that were better than me at this skill would tell me the same thing. Come to think of it, this applies to anything that you would like to learn. So I have decided to start writing at least once a week for the rest of the year. The topic can be as vague and the article can be of shoddy quality. But the important thing is to put things out there.

Therefore, I am going to use this 'nanoblog' of sorts to create posts that are writing exercises of sorts. They are mainly for myself and may not have much value for readers. But I would like to look back on this first article one day and say to myself, "Boy am I glad that I stuck to it and don't write like that anymore."